ssh acan << EOF
  cd /home/rudolf/projects/acar
  git pull
  MIX_ENV=prod mix do deps.get, release --env=prod --upgrade
  
  PROD_REL=_build/prod/rel/acar
  RELEASE_VERSION=$(ls -t $PROD_REL/releases | head -1)
  
  $PROD_REL/bin/acar upgrade $RESELASE_VERSION
EOF