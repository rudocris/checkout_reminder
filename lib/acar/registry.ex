defmodule ACAR.Registry do
  use GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts)
  end

  def top do
    case :ets.last(:acar) do
      :"$end_of_table" -> 0
      id -> id
    end
  end

  def add(id) do
    :ets.insert_new(:acar, {id})
  end

  def init(registry \\ []) do
    PersistentEts.new(:acar, "acar.tab", [:ordered_set, :named_table, :public])
    {:ok, registry}
  end
end