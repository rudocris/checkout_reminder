defmodule ACAR.Shopify do

  @doc """
  Make a get request to `resource` with optional parameters

  ## Examples

      iex> ACAR.Shopify.get("checkouts")
      {:ok, %{"checkouts" => [...]}}
  """
  def get(resource, params \\ []) do
    {api_key, password, name} = Application.get_env(:acar, :shopify_api_keys)
    uried_params = URI.encode_query(params)

    case HTTPoison.get!("https://#{api_key}:#{password}@#{name}.myshopify.com/admin/#{resource}.json?#{uried_params}") do
      %HTTPoison.Response{status_code: 200, body: body} ->
        {:ok, Jason.decode!(body)}
      %HTTPoison.Response{status_code: _, body: body} ->
        {:error, Jason.decode!(body)}
    end
  end
end