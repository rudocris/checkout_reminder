defmodule ACAR.Email do
  import Bamboo.Email

  def new_checkout(checkout) do
    from = Application.get_env(:acar, ACAR.Mailer)[:username]
    admin_email = Application.get_env(:acar, :admin_email)
    {_, _, site_name} = Application.get_env(:acar, :shopify_api_keys)

    checkout_url = "#{site_name}.myshopify.com/admin/checkouts/#{checkout["id"]}"

    %{"customer" => 
      %{
        "first_name" => first_name,
        "last_name" => last_name
      },
      "shipping_address" => 
      %{
        "address1" => address,
        "city" => city,
        "zip" => zip,
        "phone" => phone
      },
      "total_price" => total_price
    } = checkout

    html_body = """
      #{first_name} #{last_name} from #{address} #{city} <b>#{zip}</b> failed to make a purchase for <b>$#{total_price}</b>

      <br>
      Their phone number is <b>#{phone}</b>
      <br><br>

      <a href=\"#{checkout_url}\">Check it out!</a>
      
      """

    new_email(
      from: from,
      to: admin_email,
      subject: "Abandoned Checkout for $#{total_price}",
      html_body: html_body 
    )
  end
end