defmodule ACAR.Periodically do
  use GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl true
  def init(state) do
    schedule_work()
    {:ok, state}
  end
  
  @impl true
  def handle_info(:work, state) do
    ACAR.refresh()

    schedule_work()
    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, 1 * 60 * 1000) #run every minute
  end
end