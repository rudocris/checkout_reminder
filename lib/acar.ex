defmodule ACAR do
  require Logger
  @moduledoc """
  Documentation for ACAR.
  """

  alias ACAR.{Registry, Shopify, Mailer, Email}

  def refresh do
    get_checkouts_since(Registry.top)
    |> Enum.each(&notify_and_update_registry/1)
  end

  defp get_checkouts_since(id, opts \\ [])
  defp get_checkouts_since(0, opts) do
    yesterday = DateTime.utc_now
      |> Timex.shift(days: -1)
      |> Timex.format!("%FT%T%:z", :strftime)

      {:ok, %{"checkouts" => checkouts}} = Shopify.get("checkouts", [{:created_at_min, yesterday}, {:status, "open"} | opts])
      exclude_completed(checkouts)
  end
  
  defp get_checkouts_since(id, opts) do 
    {:ok, %{"checkouts" => checkouts}} = Shopify.get("checkouts", [{:since_id, id}, {:status, "open"} | opts])
    exclude_completed(checkouts)
  end

  defp exclude_completed(checkouts) do
    Enum.filter(checkouts, fn checkout -> is_nil(checkout["completed_at"]) end)  
  end

  defp notify_and_update_registry(%{"id" => id} = checkout) do
    email = Email.new_checkout(checkout)
    case Mailer.deliver_now(email) do
      %Bamboo.Email{} -> Registry.add(id)
    end
  end
end
