defmodule ACAR.MixProject do
  use Mix.Project

  def project do
    [
      app: :acar,
      version: "0.1.0-u4",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :timex, :bamboo, :bamboo_smtp],
      mod: {ACAR.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:jason, "~> 1.0"},
      {:persistent_ets, "~> 0.1.0"},
      {:timex, "~> 3.1"},
      {:bamboo, "~> 1.1"},
      {:bamboo_smtp, "~> 1.6.0"},
      {:distillery, "~> 2.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
